package test.com.retail.core;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.retail.core.CaliculateAirShipCost;
import com.retail.core.Item;
//Parameterized test for air cost
@RunWith(Parameterized.class)
public class CaliculateAirShipCostTest {
	private double expectedValue;
	private Item item;
	

	public CaliculateAirShipCostTest(double expectedValue, Item item) {
		super();
		this.expectedValue = expectedValue;
		this.item = item;
	}
	CaliculateAirShipCost airCost;
	@Before
	public void intiate() {
		airCost= new CaliculateAirShipCost();
	}
	@Test
	public void test() {
		assertEquals(expectedValue, airCost.airShippingCost(item),0.01);
	}
	@Parameters
	public static Collection<Object[]> testData(){
		Object[][] data = new Object[][] {{4.63,new Item(567321101987L, "CD � Pink Floyd, Dark Side Of The Moon" , 19.99f, 0.58 , "AIR")},
			{4.4,new Item(567321101985L , "CD � Queen, A Night at the Opera" , 20.49f, 0.55 , "AIR")},{6.57,new Item(467321101899L , "iPhone - Waterproof Case" , 9.75f, 0.73 , "AIR")}};
	return Arrays.asList(data);	
	}
		
}
